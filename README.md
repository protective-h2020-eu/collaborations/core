# Core

Shared library for the converters

### requirements: 
* **Python3.6**
* **PyMISP** python library

# MISP to IDEA
This conversion handles class **MispToIdea**. 
For the conversion itself just create new intance of the class by calling **_MispToIdea()_**. Then call it's instance method **_to_idea()_**, to which you can pass these arguments:

* **misp_event** - the MISP event itself (this argument is the only compulsory)

* **idea_id** - uuid which will be used for new IDEA message (sometimes needs to be preset)

* **test** - Adds 'Test' Category to new IDEA message

* **origdata** - adds original MISP event to IDEA attachment

For multiple conversions can be used one instance of **_MispToIdea()_**

If the converter do not convert MISP events to IDEA, it is most probably because they do not contain any Tag from RSIT or ECSIRT namespace. MISP events are converted if they contain atleast one Tag from RSIT or ECSIRT namespace, otherwise the MISP event is most probably different type of event, which should not be converted into IDEA.

# IDEA to MISP
This conversions handles class **IdeaToMisp**.
For the conversion itself just create new instance of the class by calling **_IdeaToMisp()_**. Then call it's instance method **_to_misp()_**, to which you can pass these arguments:

* **idea_event** - the IDEA event itself (this argument is the only compulsory) 

* **test** - Adds 'ecsirt:other="other"' and 'rsit:test="test"' to newly converted MISP event Tags

For multiple conversions can be used one instance of **_IdeaToMisp()_**

### Object templates
Also object templates for **Source**, **Target** and **Attach** objects has to be added to **MISP instance** for proper functionality. 

* For every template create new folder named after the object name in */app/files/misp-objects/objects* and copy its *definition.json* file into it. 

* After that, go to web interface and go to **List Object Templates** under **Global Actions** menu. Here just click on **Update Objects** in the left side bar.

Now the object templates should work properly.