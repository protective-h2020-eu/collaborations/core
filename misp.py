# -*- coding: utf-8 -*-
#
# Copyright (c) 2018, CESNET, z. s. p. o.
# Use of this source is governed by an ISC license, see LICENSE file.

"""
Conversion library for conversion between IDEA format (https://idea.cesnet.cz/en/index) and
MISP core format (https://tools.ietf.org/html/draft-dulaunoy-misp-core-format-01)
"""

from uuid import uuid4
import time
import itertools
from _datetime import datetime
import re
import logging

from pymisp import MISPEvent, MISPObject, NewAttributeError, MISPOrganisation
from pymisp import logger as pymisp_logger

# all of the used IDEA categories and their matching MISP taxonomies
category_to_taxonomy = {
    'Abusive': ['ecsirt:abusive-content', 'rsit:abusive-content'],
    'Abusive.Spam': ['ecsirt:abusive-content="spam"', 'rsit:abusive-content="spam"'],
    'Abusive.Harassment': ['ecsirt:abusive-content="harmful-speech"', 'rsit:abusive-content="harmful-speech"'],
    'Abusive.Child': ['ecsirt:abusive-content="violence"', 'rsit:abusive-content="violence"'],
    'Abusive.Sexual': ['ecsirt:abusive-content="violence"', 'rsit:abusive-content="violence"'],
    'Abusive.Violence': ['ecsirt:abusive-content="violence"', 'rsit:abusive-content="violence"'],
    'Malware': ['ecsirt:malicious-code:"malware"', 'rsit:malicious-code'],
    'Malware.Virus': ['ecsirt:malicious-code="virus"', 'rsit:malicious-code="virus"'],
    'Malware.Worm': ['ecsirt:malicious-code="worm"', 'rsit:malicious-code="worm"'],
    'Malware.Trojan': ['ecsirt:malicious-code="trojan"', 'rsit:malicious-code="trojan"'],
    'Malware.Spyware': ['ecsirt:malicious-code="spyware"', 'rsit:malicious-code="spyware"'],
    'Malware.Dialer': ['ecsirt:malicious-code="dialer"', 'rsit:malicious-code="dialer"'],
    'Malware.Rootkit': ['ecsirt:malicious-code="rootkit"', 'rsit:malicious-code="rootkit"'],
    'Malware.Ransomware': ['ecsirt:malicious-code="ransomware"', 'rsit:malicious-code'],
    'Recon': ['ecsirt:information-gathering', 'rsit:information-gathering'],
    'Recon.Scanning': ['ecsirt:information-gathering="scanner"', 'rsit:information-gathering="scanner"'],
    'Recon.Sniffing': ['ecsirt:information-gathering="sniffing"', 'rsit:information-gathering="sniffing"'],
    'Recon.SocialEngineering': ['ecsirt:information-gathering="social-engineering"',
                                'rsit:information-gathering="social-engineering"'],
    'Recon.Searching': ['ecsirt:information-gathering', 'rsit:information-gathering'],
    'Attempt': ['ecsirt:intrusion-attempts', 'rsit:intrusion-attempts'],
    'Attempt.Exploit': ['ecsirt:intrusion-attempts="exploit"', 'ecsirt:intrusion-attempts="ids-alert"',
                        'rsit:intrusion-attempts="ids-alert"', 'rsit:intrusion-attempts="exploit"'],
    'Attempt.Login': ['ecsirt:intrusion-attempts="brute-force"', 'rsit:intrusion-attempts="brute-force"'],
    'Attempt.NewSignature': ['ecsirt:intrusion-attempts', 'rsit:intrusion-attempts'],
    'Intrusion': ['ecsirt:intrusions', 'rsit:intrusions'],
    'Intrusion.AdminCompromise': ['ecsirt:intrusions="unprivileged-account-compromise"',
                                  'rsit:intrusions="unprivileged-account-compromise"'],
    'Intrusion.UserCompromise': ['ecsirt:intrusions="unprivileged-account-compromise"',
                                 'rsit:intrusions="unprivileged-account-compromise"'],
    'Intrusion.AppCompromise': ['ecsirt:intrusions="application-compromise"',
                                'rsit:intrusions="application-compromise"'],
    'Intrusion.Botnet': ['ecsirt:intrusions="bot"', 'rsit:intrusions="bot"'],
    'Availability': ['ecsirt:availability', 'rsit:availability'],
    'Availability.DoS': ['ecsirt:availability="dos"', 'rsit:availability="dos"'],
    'Availability.DDoS': ['ecsirt:availability="ddos"', 'rsit:availability="ddos"'],
    'Availability.Sabotage': ['ecsirt:availability="sabotage"', 'rsit:availability="sabotage"'],
    'Availability.Outage': ['ecsirt:availability="outage"', 'rsit:availability="outage"'],
    'Information': ['ecsirt:information-content-security', 'rsit:information-content-security'],
    'Information.UnauthorizedAccess': ['ecsirt:information-content-security="Unauthorised-information-access"',
                                       'rsit:information-content-security="Unauthorised-information-access"'],
    'Information.UnauthorizedModification':
                                        ['ecsirt:information-content-security="Unauthorised-information-modification"',
                                         'rsit:information-content-security="Unauthorised-information-modification"'],
    'Fraud': ['ecsirt:fraud', 'rsit:fraud'],
    'Fraud.UnauthorizedUsage': ['ecsirt:fraud="unauthorized-use-of-resources"',
                                'rsit:fraud="unauthorized-use-of-resources"'],
    'Fraud.Copyright': ['ecsirt:fraud="copyright"', 'rsit:fraud="copyright"'],
    'Fraud.Masquerade': ['ecsirt:fraud="masquerade"', 'rsit:fraud="masquerade"'],
    'Fraud.Phishing': ['ecsirt:fraud="phishing"', 'rsit:fraud="phishing"'],
    'Fraud.Scam': ['ecsirt:fraud', 'rsit:fraud'],
    'Vulnerable': ['ecsirt:vulnerable', 'rsit:vulnerable'],
    'Vulnerable.Open': ['ecsirt:vulnerable="vulnerable-service"', 'rsit:vulnerable="vulnerable-service"'],
    'Vulnerable.Config': ['ecsirt:vulnerable="vulnerable-service"', 'rsit:vulnerable="vulnerable-service"'],
    'Anomaly': ['ecsirt:other="other"', 'rsit:other="other"'],
    'Anomaly.Traffic': ['ecsirt:other="other"', 'rsit:other="other"'],
    'Anomaly.Connection': ['ecsirt:other="other"', 'rsit:other="other"'],
    'Anomaly.Protocol': ['ecsirt:other="other"', 'rsit:other="other"'],
    'Anomaly.System': ['ecsirt:other="other"', 'rsit:other="other"'],
    'Anomaly.Application': ['ecsirt:other="other"', 'rsit:other="other"'],
    'Anomaly.Behaviour': ['ecsirt:other="other"', 'rsit:other="other"'],
    'Other': ['ecsirt:other="other"', 'rsit:other="other"'],
    'Test': ['ecsirt:test="test"', 'rsit:test="test"']
}


class MispToIdea(object):
    """
    Converts MISP event in MISP core format to IDEA event
    """

    useful_hashes = ["md5", "sha1", "sha224", "sha256", "sha384", "sha512", "sha512/224", "sha512/256", "filename|md5",
                     "filename|sha1", "filename|sha224", "filename|sha256", "filename|sha384", "filename|sha512",
                     "filename|sha512/224", "filename|sha512/256"]
    useful_ip_objects = ["ip-port", "domain-ip", "netflow", "network-connection", "network-socket", "cowrie", "ddos",
                         "fail2ban", "research-scanner"]
    _taxonomy_to_category_created = False
    taxonomy_to_category = {}

    def __init__(self):
        """
        Initializes class and prepares instance variables
        """
        if not self._taxonomy_to_category_created:
            # create taxonomy_to_category conversion dictionary
            for idea_category, misp_taxonomies in category_to_taxonomy.items():
                for misp_tag in misp_taxonomies:
                    self.taxonomy_to_category[misp_tag] = idea_category
            self._taxonomy_to_category_created = True

        # prepare helpful variables
        self._attach_counter = 0
        self._source = []
        self._target = []
        self._attach = []
        self._idea_attribs = {}
        self._logger = logging.getLogger("misp.py-MispToIdea")

    @staticmethod
    def convert_epoch_to_utc(timestamp):
        """
        Converts Unix timestamp to to UTC datetime
        :param timestamp: Unix timestamp
        :return: UTC datetime in string
        """
        return time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime(int(timestamp)))

    @staticmethod
    def get_date_from_timestamp(timestamp):
        date_and_time = MispToIdea.convert_epoch_to_utc(timestamp)
        # returns only date from datetime, date stops at index 9 (2018-05-16T06:20:35Z)
        return date_and_time[0:9]

    @staticmethod
    def get_ip_version(ip_addr):
        """
        Gets verstion of IP address
        :param ip_addr: the IP address
        :return: version of IP address
        """
        return "IP6" if ":" in ip_addr else "IP4"

    @staticmethod
    def get_ip_addr(attrib, ip_only=True):
        """
        Gets IP address value from event's attribute
        :param attrib: the attribute
        :param ip_only: gets only IP address
        :return: returns IP address, role of IP address (Source|Target) and if the attributes is  composite, the port or
        hostname may be returned in addition. Only IP address returned if ip_only == True
        """
        role = "Source" if "src" in attrib['type'] else "Target"
        if "ip-src" == attrib['type'] or "ip-dst" == attrib['type']:
            return attrib['value'] if ip_only is True else (attrib['value'], role, None, None)
        elif "ip-src|port" == attrib['type'] or "ip-dst|port" == attrib['type']:
            split_attrib = attrib['value'].split('|')
            if len(split_attrib) == 1:
                split_attrib = attrib['value'].split(':')
            return split_attrib[0] if ip_only else (split_attrib[0], role, split_attrib[1], None)
        else:
            # attrib['type'] == "domain|ip"
            split_attrib = attrib['value'].split('|')
            return split_attrib[1] if ip_only else (split_attrib[1], "Target", None, split_attrib[0])

    @classmethod
    # https://stackoverflow.com/questions/9807634/find-all-occurrences-of-a-key-in-nested-python-dictionaries-and-lists
    def find(cls, key, value):
        """
        find gradually all values by key
        :param key: searched key
        :param value: object which is being searched in (dict|list)
        :return: found value by key
        """
        if isinstance(value, dict):
            for k, v in value.items():
                if k == key:
                    yield v
                elif isinstance(v, dict):
                    for result in cls.find(key, v):
                        yield result
                elif isinstance(v, list):
                    for d in v:
                        for result in cls.find(key, d):
                            yield result
        elif isinstance(value, list):
            for v in value:
                for result in cls.find(key, v):
                    yield result

    def process_misp_attribute(self, attrib, idea_categories):
        """
        Process MISP attribute and get all useful data from it and insert it into corresponding IDEA object list
        :param attrib: the attribute, which will be processed
        :param idea_categories: categories, which were already assigned to converted event
        :return: None
        """
        # if attribute contains ip address, get it with other potential attributes(port, hostname) and append it to
        # Source or Target based on IP address type
        if "ip" in attrib['type']:
            ip_addr, role, port, hostname = self.get_ip_addr(attrib, False)
            new_description = {self.get_ip_version(ip_addr): [ip_addr]}
            if port:
                new_description['Port'] = [int(port)]
            if hostname:
                new_description['Hostname'] = [hostname]
            if role == "Source":
                self._source.append(new_description)
            if role == "Target":
                self._target.append(new_description)

        # if attrib is hash, create new attach
        elif attrib['type'] in MispToIdea.useful_hashes:
            new_attach = {'Handle': "attach" + str(self._attach_counter)}
            self._attach_counter += 1
            if "|" in attrib['type']:
                new_attach['FileName'] = [attrib['value'].split("|")[0]]
                new_attach['Hash'] = [attrib['type'].split("|")[1] + ":" + attrib['value'].split("|")[1]]
            else:
                new_attach['Hash'] = [attrib['type'] + ":" + attrib['value']]
            self._attach.append(new_attach)

        elif attrib['type'] == "url":
            new_attach_name = "attach" + str(self._attach_counter)
            self._attach_counter += 1
            self._attach.append({'Handle': new_attach_name, 'Ref': [attrib['value']]})

        elif attrib['type'] == "email-src":
            self._source.append({'Email': [attrib['value']]})

        elif attrib['type'] == "email-dst":
            self._target.append({'Email': [attrib['value']]})

        elif attrib['type'] == "comment" and attrib['category'] in ("Support tool", "Internal reference"):
            try:
                self._idea_attribs['Note'] += attrib['value'] + ". "
            except KeyError:
                self._idea_attribs['Note'] = attrib['value'] + ". "
        elif attrib['type'] in ("domain", "hostname") and attrib['category'] == "Network activity" and "Fraud.Phishing" in idea_categories:
            self._source.append({'Hostname': [attrib['value']]})

    @staticmethod
    def append_value_or_create_list(key, value, idea_object):
        """
        Append value to array retrieved by key from idea_object, if key does not exist, create key in idea_object
        and assign array with inserted value to this key
        :param key: search key
        :param value: appended value
        :param idea_object: updated object
        :return: None
        """
        try:
            idea_object[key].append(value)
        except KeyError:
            idea_object[key] = [value]

    @staticmethod
    def convert_org_name(misp_name):
        """
        Convert MISP Organization name into IDEA-compatible name. It can only
        contain lowercase letters, numbers and underscores.
        :param misp_name: name of misp organization
        :return: None
        """
        # Replace any disallowed character (or mulitple consecutive ones) by 
        # a singe underscore
        name = re.sub('[^a-zA-Z0-9_]+', '_', misp_name).strip('_').lower()
        # It mustn't start with a number (add underscore to the beginning)
        if name[0].isdigit():
            name = '_' + name
        return name

    def add_attach(self, value, content_type, source=None, target=None):
        """
        Creates new Attach object and creates references to Source and Target objects if needed
        :param value: value of the attachment
        :param content_type: type of the attachment
        :param source: Source object, where will be the reference inserted
        :param target: Target object, where will be the reference inserted
        :return: None
        """
        attach = {}
        # create reference/id
        attach_id = "att" + str(self._attach_counter)
        attach['Handle'] = attach_id
        if source is not None:
            self.append_value_or_create_list("AttachHand", attach_id, source)
        if target is not None:
            self.append_value_or_create_list("AttachHand", attach_id, target)
        self._attach_counter += 1
        # insert content_type and value
        attach['Type'] = [content_type]
        attach['Content'] = value
        # append it to already created attachments
        self._attach.append(attach)

    def process_misp_object(self, misp_object):
        """
        Process MISP object and get all useful data from it and insert it into corresponding IDEA object array
        :param misp_object: the object, which will be processed
        :return: None
        """
        # if object name is in useful_ip_objects, all its attributes will be filled into source or target
        if misp_object['name'] in MispToIdea.useful_ip_objects:
            source = {}
            target = {}
            proto_dict = {}
            for attrib in misp_object['Attribute']:
                if attrib['type'] == "ip-dst":
                    ip_addr = self.get_ip_addr(attrib)
                    self.append_value_or_create_list(self.get_ip_version(ip_addr), ip_addr, target)
                elif attrib['type'] == "ip-src":
                    ip_addr = self.get_ip_addr(attrib)
                    self.append_value_or_create_list(self.get_ip_version(ip_addr), ip_addr, source)
                elif attrib['type'] in ("src-port", "port") or attrib['object_relation'] == "src-port":
                    self.append_value_or_create_list("Port", int(attrib['value']), source)
                elif attrib['type'] in ("dst-port", "port") or attrib['object_relation'] == "dst-port":
                    self.append_value_or_create_list("Port", int(attrib['value']), target)
                elif attrib['object_relation'] == "hostname-src":
                    self.append_value_or_create_list("Hostname", attrib['value'], source)
                elif attrib['object_relation'] == "hostname-dst" or attrib['type'] == "hostname":
                    self.append_value_or_create_list("Hostname", attrib['value'], target)
                elif attrib['object_relation'] == "protocol":
                    source['Proto'] = [attrib['value']]
                    target['Proto'] = [attrib['value']]
                elif attrib['object_relation'].startswith("layer"):
                    # means layer3-protocol, layer4-protocol or layer7-protocol
                    # save its value to number of layer
                    proto_dict[attrib['object_relation'][5]] = attrib['value']
                elif attrib['type'] == "src-as" or attrib['object_relation'] == "src-as":
                    source['ASN'] = [int(attrib['value'])]
                elif attrib['type'] == "dst-as" or attrib['object_relation'] == "dst-as":
                    target['ASN'] = [int(attrib['value'])]
                elif attrib['type'] == "domain" and misp_object['name'] != "research-scanner":
                    # won't overwrite previous value of hostname-dst. Hostname-dst can occur in object of type
                    # network-connection and network-socket, but domain occurs only in other object types
                    # multiple domains can be used too
                    self.append_value_or_create_list("Hostname", attrib['value'], target)
                elif attrib['type'] == "domain" and misp_object['name'] == "research-scanner":
                    self.append_value_or_create_list("Hostname", attrib['value'], source)
                elif attrib['object_relation'] == "flow-count":
                    self._idea_attribs['FlowCount'] = int(attrib['value'])
                elif attrib['object_relation'] == "packet-count":
                    self._idea_attribs['PacketCount'] = int(attrib['value'])
                elif attrib['object_relation'] == "byte-count":
                    self._idea_attribs['ByteCount'] = int(attrib['value'])
                elif attrib['type'] == "target-email":
                    target['Email'] = [attrib['value']]

            if proto_dict:
                # sort protocols by layer
                proto_list = [proto_dict.get("3", "")] + [proto_dict.get("4", "")] + [proto_dict.get("7", "")]
                # remove empty strings from proto list
                proto_list = [proto for proto in proto_list if proto != ""]
                source['Proto'] = proto_list
                target['Proto'] = proto_list

            # save gathered data
            if source:
                self._source.append(source)
            if target:
                self._target.append(target)
        # process file object
        elif misp_object['name'] == "file":
            new_attach = {'Handle': "attach" + str(self._attach_counter)}
            self._attach_counter += 1
            for attrib in misp_object['Attribute']:
                if attrib['type'] in MispToIdea.useful_hashes:
                    try:
                        new_attach['Hash'].append(attrib['type'] + ":" + attrib['value'])
                    except KeyError:
                        new_attach['Hash'] = [attrib['type'] + ":" + attrib['value']]
                elif attrib['type'] == "filename":
                    try:
                        new_attach['FileName'].append(attrib['value'])
                    except KeyError:
                        new_attach['FileName'] = [attrib['value']]
            if new_attach.get('Hash') or new_attach.get('FileName'):
                self._attach.append(new_attach)
        # process email object
        elif misp_object['name'] == "email":
            source = {}
            target = {}
            for attrib in misp_object['Attribute']:
                if attrib['type'] == "email-dst":
                    self.append_value_or_create_list('Email', attrib['value'], target)
                elif attrib['object_relation'] == "from":
                    self.append_value_or_create_list('Email', attrib['value'], source)
                elif attrib['type'] == "ip-src":
                    self.append_value_or_create_list('IP4', attrib['value'], source)
                elif attrib['type'] == "email-body":
                    self.add_attach(attrib['value'], "EmailBody", source, target)
                elif attrib['type'] == "email-attachment":
                    self.add_attach(attrib['value'], "EmailAttachment", source, target)
                elif attrib['type'] == "email-subject":
                    self.add_attach(attrib['value'], "EmailSubject", source, target)
            if source:
                self._source.append(source)
            if target:
                self._target.append(target)

    def process_source_or_target_object(self, misp_object):
        """
        Process source or target object, and retrieved data insert to IDEA source or target object and append it to
        corresponding object array
        :param misp_object: the object, which will be processed
        :return: None
        """
        converted_object = {}
        for misp_attrib in misp_object['Attribute']:
            if misp_attrib['object_relation'] not in ("Note", "Vulnerability", "Reference"):
                # other attributes than Note, Vulnerability and Reference are inserted as list in IDEA
                self.append_value_or_create_list(misp_attrib['object_relation'], misp_attrib['value'], converted_object)
            else:
                if misp_attrib['object_relation'] == "Note":
                    # Note is inserted as plain string
                    converted_object['Note'] = misp_attrib['value']
                # Vulnerability and Reference are inserted into Ref
                elif misp_attrib['object_relation'] == "Vulnerability":
                    self.append_value_or_create_list("Ref", "cve:" + misp_attrib['value'],
                                                     converted_object)
                elif misp_attrib['object_relation'] == "Reference":
                    self.append_value_or_create_list("Ref", misp_attrib['value'], converted_object)
        # save gathered data
        if misp_object['name'] == "network-source":
            self._source.append(converted_object)
        else:
            self._target.append(converted_object)

    def process_attach_object(self, misp_object):
        """
        Process attach object, and retrieved data insert to IDEA attach object and append it to attach array
        :param misp_object: the object, which will be processed
        :return: None
        """
        converted_object = {"Handle": "att" + str(self._attach_counter)}
        self._attach_counter += 1
        for attach_attrib in misp_object['Attribute']:
            if attach_attrib['object_relation'] in ("Note", "ContentType", "ContentCharset", "ContentEncoding",
                                                    "Content", "Handle"):
                converted_object[attach_attrib['object_relation']] = attach_attrib['value']
            elif attach_attrib['object_relation'] in ("FileName", "Type"):
                self.append_value_or_create_list(attach_attrib['object_relation'], attach_attrib['value'],
                                                 converted_object)
            elif attach_attrib['object_relation'] == "Reference":
                self.append_value_or_create_list("Ref", attach_attrib['value'], converted_object)
            elif attach_attrib['object_relation'] == "Vulnerability":
                self.append_value_or_create_list("Ref", "cve" + attach_attrib['value'], converted_object)
            elif attach_attrib['object_relation'] == "Size":
                converted_object['Size'] = int(attach_attrib['value'])
            else:
                self.append_value_or_create_list("Hash", attach_attrib['type'] + ":" + attach_attrib['value'],
                                                 converted_object)
        self._attach.append(converted_object)

    def convert_tags_to_categories(self, misp_tags, idea_event):
        """
        Converts MISP tags/taxonomies to IDEA categories
        :param misp_tags: list of MISP tags, which will be converted
        :param idea_event: idea event object to which will be new categories inserted into
        :return: None
        """
        # this set is for deduplication of main categories. With this set category list like
        # ['Malware', 'Malware.Virus'] will not occur
        used_main_categories = set()
        for tag in misp_tags:
            try:
                new_category = self.taxonomy_to_category[tag['name']]
                if new_category not in idea_event['Category'] and new_category not in used_main_categories:
                    idea_event['Category'].append(new_category)
                    used_main_categories.add(new_category.split('.')[0])
            except KeyError:
                pass

    def to_idea(self, misp_event, idea_id=None, test=False, misp_url=None):
        """
        Creates whole IDEA message from MISP event
        :param misp_event: the misp event
        :param idea_id: uuid of IDEA message (when needs to be preset)
        :param test: add Test into IDEA['Category']
        :param misp_url: URL of connected MISP instance
        :return: new converted IDEA message
        """
        self._logger.debug("Starting conversion of MISP event {id} to IDEA!".format(id=misp_event['id']))
        idea_event = {
            'Format': "IDEA0",
            'ID': str(idea_id) if idea_id is not None else str(uuid4()),
            'Category': [],
            'Description': misp_event['info']
        }

        # fill in the IDEA event Category
        self.convert_tags_to_categories(misp_event.get('Tag', []), idea_event)

        # cannot determine IDEA Category, cannot convert
        if not idea_event['Category']:
            self._logger.debug("Conversion of MISP event {id} was not successful, MISP event does not contain any Tag "
                               "from ECSIRT or RSIT namespace!".format(id=misp_event['id']))
            return None
        else:
            self._logger.debug("Conversion of taxonomy to category of MISP event {id} was successful!".format(
                id=misp_event['id']))

        if test and "Test" not in idea_event['Category']:
            idea_event['Category'].append("Test")

        idea_event['CreateTime'] = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")

        timestamps = itertools.chain(self.find('timestamp', misp_event['Attribute']),
                                     self.find('timestamp', misp_event['Object']))
        try:
            oldest_timestamp = min(map(int, timestamps))
            idea_event['DetectTime'] = self.convert_epoch_to_utc(oldest_timestamp)
        except ValueError:
            # no object and attributes --> min() ValueError for empty sequence
            idea_event['DetectTime'] = misp_event['date'] + "T00:00:00Z"

        # fill in info about organizations
        idea_event['Node'] = [
            {
                # TODO: it would be better if the convertor has its own name
                # and the MISP org. and the convertor would have separate Node entries.
                # But it would need a way to specify the convertor name.
                'Name': 'ext.misp.' + self.convert_org_name(misp_event['Orgc']['name']),
                'SW': ['MISP', 'MISP-to-IDEA'],
                'Type': ['Relay'],
                'Note': "MISP organization name (UUID): {} ({})".format(misp_event['Orgc']['name'], misp_event['Orgc']['uuid']),
            },
#             {
#                 'Name': 'ext.misp.' + self.convert_org_name(misp_event['Org']['name']),
#                 'Note': "MISP organization name (UUID): {} ({})".format(misp_event['Org']['name'], misp_event['Org']['uuid']),
#             },
            ]

        if misp_url and misp_event.get('Galaxy'):
            # if misp url and galaxy, fill these into IDEA Note
            notes = []
            for galaxy in misp_event['Galaxy']:
                for galaxy_cluster in galaxy.get('GalaxyCluster', []):
                    notes.append(galaxy['name'] + ": " + galaxy_cluster['value'])
            idea_event['Note'] = "This event was converted from MISP instance: " + misp_url + \
                                 ". Event helpful keywords: [" + ", ".join(notes) + "]. "
        elif misp_url:
            idea_event['Note'] = "This event was converted from MISP instance: " + misp_url + ". "

        # check all attributes for all potentially useful data
        for attrib in misp_event['Attribute']:
            self.process_misp_attribute(attrib, idea_event['Category'])

        # check all objects for all potentially useful data
        for misp_object in misp_event['Object']:
            if misp_object['name'] in ("network-source", "network-target"):
                # process the object
                self.process_source_or_target_object(misp_object)
            elif misp_object['name'] == "attach":
                self.process_attach_object(misp_object)
            else:
                self.process_misp_object(misp_object)

        # fill in the Source, Target and Attach objects, which were already converted
        if self._source:
            idea_event['Source'] = self._source
        if self._target:
            idea_event['Target'] = self._target
        if self._attach:
            idea_event['Attach'] = self._attach

        # insert original MISP event into IDEA Attach object
        origdata = {
            'Handle': "att" + str(self._attach_counter),
            'Note': "original data",
            'Content': misp_event
        }
        self.append_value_or_create_list('Attach', origdata, idea_event)

        # insert root attributes, if there was some created in conversion process
        for idea_key, idea_value in self._idea_attribs.items():
            if idea_key == "Note":
                idea_event[idea_key] += idea_value
            else:
                idea_event[idea_key] = idea_value

        # prepare instance for another conversion
        self.__init__()

        self._logger.debug("Conversion of MISP event {id} to IDEA was successful!".format(id=misp_event['id']))

        return idea_event


class IdeaToMisp(object):
    """
    Converts IDEA event to MISP event in MISP core format
    """

    re_cve = re.compile("cve:", re.IGNORECASE)
    severity_conversion = {
        "low": 3,
        "medium": 2,
        "high": 1,
        "critical": 1
    }
    object_name_translation = {
        'Source': "network-source",
        'Target': "network-target",
        'Attach': "attach"
    }

    def __init__(self):
        """
        Initializes class and prepares instance variables
        """
        self._idea_event = None
        self._new_event = MISPEvent()
        self._origdata = None
        self._logger = logging.getLogger("misp.py-IdeaToMisp")

    @staticmethod
    def get_organization(misp_event):
        """
        Get organizations's informations from original event and return it as MISPOrganization object
        :param misp_event: misp event, from which are informations about organization taken from
        :return: None if data are damaged or correct instance of MISPOrganization object
        """
        # get organisation's name and uuid, if not found, original event record is damaged
        try:
            orgc_name = misp_event['Orgc']['name']
            orgc_uuid = misp_event['Orgc']['uuid']
        except KeyError:
            return None

        misp_orgc = MISPOrganisation()
        misp_orgc.name = orgc_name
        misp_orgc.uuid = orgc_uuid
        return misp_orgc

    def copy_original_event(self):
        """
        If original MISP event was found in IDEA Attach, then try to reconstruct the original MISP event
        :return: None
        """
        # set distribution, threat_level_id, analysis
        self._new_event.distribution = int(self._origdata.get('distribution', 1))
        self._new_event.threat_level_id = int(self._origdata.get('threat_level_id', 4))
        self._new_event.analysis = 2
        # [0:10] is for taking date only from DetectTime (2019-01-26xxxxxxx), should be same if origdata present
        self._new_event.date = datetime.strptime(self._origdata.get('date'), "%Y-%m-%d")
        # set info and all original tags
        self._new_event.info = self._origdata.get('info')
        for original_tag in self._origdata.get('Tag', []):
            self._new_event.add_tag(original_tag['name'])
        # copy all original attributes
        for original_attrib in self._origdata.get('Attribute', []):
            self._new_event.add_attribute(type=original_attrib['type'], category=original_attrib['category'],
                                          value=original_attrib['value'])
        # copy all original objects
        for original_object in self._origdata.get('Object', []):
            new_object = MISPObject(name=original_object['name'])
            for object_attrib in original_object['Attribute']:
                new_object.add_attribute(object_relation=object_attrib['object_relation'], value=object_attrib['value'],
                                         type=object_attrib['type'], category=object_attrib['category'])
            self._new_event.add_object(new_object)

        for original_galaxy in self._origdata.get('Galaxy', []):
            # PyMISP does not have method for adding galaxy, but in GITHUB issues they say that it should work by just
            # adding it as tag
            self._new_event.add_tag(original_galaxy['name'])

        # copy original organization and UUID of event, if possible
        orgc = self.get_organization(self._origdata)
        if orgc:
            self._new_event.orgc = orgc
        event_uuid = self._origdata.get('uuid')
        if event_uuid:
            self._new_event.uuid = event_uuid

    def process_basic_info(self, test):
        """
        Process basic info of MISP event such threat_level_id, info, Tags, and standalone attributes
        :param test: Add Test tag to 'Tags' if True
        :return: None
        """
        # add basic info about event
        self._new_event.distribution = 1

        try:
            self._new_event.threat_level_id = \
                IdeaToMisp.severity_conversion[self._idea_event['_CESNET']['EventSeverity'].lower()]
        except KeyError:
            # EventSeverity not in IDEA message --> 4 = Undefined (Docs says 0, but in implementation it is vice versa)
            self._new_event.threat_level_id = 4

        self._new_event.analysis = 2
        # [0:10] is for taking date only from DetectTime (2019-01-26xxxxxxx), should be same even if origdata present
        self._new_event.date = datetime.strptime(self._idea_event['DetectTime'][0:10], "%Y-%m-%d")

        if self._idea_event.get('Description'):
            # add Description as event info and add Note as well if present
            try:
                self._new_event.info = self._idea_event['Description']
                self._new_event.add_attribute(type="comment", value=self._idea_event['Note'],
                                              category="Internal reference", disable_correlation=True)
            except KeyError:
                self._new_event.info = self._idea_event['Description']
        elif self._idea_event.get('Note'):
            # or atleast Note if Description is not present
            self._new_event.info = self._idea_event['Note']
        else:
            # if no info about event present, add the first Node Name with the first category of the IDEA event at
            # least
            self._new_event.info = self._idea_event['Node'][0]['Name'] + " - " + self._idea_event['Category'][0]

        # fill in tags of new event
        self._new_event.add_tag("tlp:green")
        # pop Test category from IDEA event categories for intersection purposes
        idea_event_categories = self._idea_event['Category']
        try:
            idea_event_categories.remove('Test')
        except ValueError:
            pass
        # do intersection of known IDEA categories and categories in the converted event
        # if the intersection is empty, then the category is unknown --> fill in 'other' category
        if not [known_idea_category for known_idea_category in idea_event_categories if
                known_idea_category in category_to_taxonomy.keys()]:
            self._new_event.add_tag("ecsirt:other='other'")
            self._new_event.add_tag("rsit:other='other'")
        else:
            # convert all categories in IDEA to taxonomies
            for idea_category in self._idea_event['Category']:
                # add taxonomies ECSIRT and RSIT
                for misp_taxonomy in category_to_taxonomy[idea_category]:
                    self._new_event.add_tag(misp_taxonomy)

        # add test taxonomies if desired
        if test and "Test" not in self._idea_event['Category']:
            self._new_event.add_tag('ecsirt:test="test"')
            self._new_event.add_tag('rsit:test="test"')

        for reference in self._idea_event.get('Ref', []):
            # if reference is cve, add MISP attribute as vulnerability, otherwise add MISP attribute as link
            if IdeaToMisp.re_cve.search(reference):
                self._new_event.add_attribute(category="External analysis", type="vulnerability",
                                              value=IdeaToMisp.re_cve.split(reference)[1], disable_correlation=True)
            else:
                self._new_event.add_attribute(category="External analysis", type="link",
                                              value=reference, disable_correlation=True)

    @classmethod
    def add_attribute_to_object(cls, key, value, misp_object):
        """
        Add one value (new MISP attribute) to source or target or attach object
        :param key: name of attribute relation
        :param value: inserted value
        :param misp_object: object, which will the attribute be inserted into
        :return: the object with inserted attribute
        """
        # disabling pymisp logger, because it logs Warning when catching NewAttributeError, which may be confusing in
        # log, because the error is properly catched here
        pymisp_logger.disabled = True
        try:
            misp_object.add_attribute(key, value=value)
        except NewAttributeError:
            # added attribute is not in the template, it has to be either Ref attribute or Hash in case of Attach object
            if key == "Ref":
                if IdeaToMisp.re_cve.search(value):
                    misp_object.add_attribute("Vulnerability", value=cls.re_cve.split(value)[1])
                else:
                    misp_object.add_attribute("Reference", value=value)
            if misp_object.name == "attach" and key == "Hash":
                hash_method = value.split(":", 1)[0]
                hash_value = value.split(":", 1)[1]
                try:
                    misp_object.add_attribute(hash_method.lower(), value=hash_value)
                except NewAttributeError:
                    # Trying to insert value of hash algorithm, which is not defined in template
                    pass
        pymisp_logger.disabled = False
        return misp_object

    def process_one_idea_object(self, object_name):
        """
        Process IDEA's Source or Target or Attach and convert it into MISP object
        :param object_name: defines processing of IDEA's 'Source' or 'Target' or 'Attach'
        :return: None
        """
        for idea_obj in self._idea_event.get(object_name, []):
            new_object = MISPObject(name=IdeaToMisp.object_name_translation[object_name], strict=True, standalone=False,
                                    misp_objects_path_custom='object_templates')
            for key, value in idea_obj.items():
                # if values are in the list, process them one by one
                if isinstance(value, list):
                    for list_value in value:
                        new_object = self.add_attribute_to_object(key, list_value, new_object)
                # or the values is just strict, process it
                else:
                    new_object = self.add_attribute_to_object(key, value, new_object)
            # save gathered data
            self._new_event.add_object(new_object)

    def process_idea_node(self):
        """
        Processes Node object by copying its names and SW values to MISP comment attribute
        :return: None
        """
        detection_sw_list = []
        detector_name_list = []
        for node_obj in self._idea_event.get('Node', []):
            try:
                detector_name_list.append(node_obj['Name'])
            except KeyError:
                pass
            for sw_name in node_obj.get('SW', []):
                detection_sw_list.append(sw_name)
        detector_name = "Detector name(s): " + ", ".join(detector_name_list)
        detection_sw = "Detection software(s): " + ", ".join(detection_sw_list)
        self._new_event.add_attribute(type="comment", category="Internal reference", value=detector_name,
                                      disable_correlation=True)
        self._new_event.add_attribute(type="comment", category="Internal reference", value=detection_sw,
                                      disable_correlation=True)

    def process_all_idea_objects(self):
        """
        Calls processing of Source, Target, Attach and Node objects
        :return: None
        """
        self.process_one_idea_object('Source')
        self._logger.debug("All Source objects of IDEA event {id} are converted!".format(id=self._idea_event['ID']))
        self.process_one_idea_object('Target')
        self._logger.debug("All Target objects of IDEA event {id} are converted!".format(id=self._idea_event['ID']))
        self.process_one_idea_object('Attach')
        self._logger.debug("All Attach objects of IDEA event {id} are converted!".format(id=self._idea_event['ID']))
        self.process_idea_node()

    def check_origdata(self):
        for attach in self._idea_event.get("Attach", []):
            if attach.get('Note', "") == "original data":
                # found original data, but still should be checked, if the original data are from MISP
                # keys Org and Orgc should be unique for MISP platform, if found, then the original event is from MISP
                if attach.get('Content', {}).get('Org') and attach.get('Content', {}).get('Orgc'):
                    self._origdata = attach['Content']
                    return True
        return False

    def to_misp(self, idea_event, test=False):
        """
        Converts IDEA message into MISP core format message
        :param idea_event: the idea event, which will be converted
        :param test: add Test tag to converted event (conversion for testing purposes)
        :return: instance of MISPEvent (with converted data)
        """
        self._logger.debug("Starting conversion of IDEA event {id} to MISP!".format(id=idea_event['ID']))
        # prepare instance for another conversion (for first conversion it is not needed, but for others yes)
        self.__init__()
        self._idea_event = idea_event
        # check if event does not have attached original MISP event (if event comes from MISP)
        if self.check_origdata():
            self._logger.debug("Found original MISP event, converter will try to reconstruct original MISP event!")
            self.copy_original_event()
        else:
            # fill new event with basic info and set event as published
            self.process_basic_info(test)
            self._logger.debug("Basic info of IDEA event {id} is converted!".format(id=idea_event['ID']))
            # process IDEA objects Source, Target, Attach
            self.process_all_idea_objects()
            # add tag and try to find original data, if the event is from other MISP instance

        # self._new_event is instance of MISPEvent()
        self._new_event.publish()
        self._logger.debug("Conversion of IDEA event {id} was successful, MISP event is ready to be pushed to MISP "
                           "instace!".format(id=idea_event['ID']))
        return self._new_event
