#!/bin/bash

set -ex

IMAGE_NAME="protective-h2020-eu/collaborations/core"
TAG="${1}"

REGISTRY="registry.gitlab.com"

docker build -t ${REGISTRY}/${IMAGE_NAME}:${TAG} .

docker push ${REGISTRY}/${IMAGE_NAME}:${TAG}
